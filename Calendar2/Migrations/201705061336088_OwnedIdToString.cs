namespace Calendar2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OwnedIdToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "OwnerId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "OwnerId", c => c.Int(nullable: false));
        }
    }
}
