﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calendar2.Models
{
    public class Event
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public string Place { get; set; }
        public string Description { get; set; }
        public string OwnerId { get; set; }
    }
}