﻿using System.Web.Optimization;

namespace Calendar2
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/vendor/js/angular.min.js",
                "~/vendor/js/angular-route.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/vendor/js/moment.min.js",
                "~/vendor/js/bootstrap.min.js",
                "~/vendor/js/angularjs-datetime-picker.min.js",
                "~/vendor/js/jquery-1.11.1.min.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/bootstrap.css").Include(
                "~/vendor/css/lib/bootstrap.min.css",
                "~/vendor/css/lib/angularjs-datetime-picker.css",
                "~/vendor/css/lib/bootstrap-theme.min.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app.js").Include(
                "~/CalendarApp/App.js",
                "~/CalendarApp/Controllers/DayController.js",
                "~/CalendarApp/Controllers/WeekController.js",
                "~/CalendarApp/Controllers/MonthController.js",
                "~/CalendarApp/Controllers/YearController.js",
                "~/CalendarApp/Controllers/AuthController.js",
                "~/CalendarApp/Controllers/UserEventController.js",
                "~/CalendarApp/Controllers/NotFoundController.js",

                "~/CalendarApp/Services/AuthService.js",

                "~/CalendarApp/Services/UserEventsStorageService.js",
                "~/CalendarApp/Services/UserEventsService.js",

                "~/CalendarApp/Services/HolidayStorageService.js",
                "~/CalendarApp/Services/HolidayService.js",

                "~/CalendarApp/Services/HelperService.js",
                "~/CalendarApp/Services/NotificationService.js",

                "~/CalendarApp/Directives/HeadDirective.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/app.css").Include(
                "~/vendor/css/style.css")
            );
        }
    }
}
