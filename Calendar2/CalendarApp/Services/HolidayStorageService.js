﻿app.service("HolidayStorageService", function ($q) {
    var OBJECT_STORE_NAME = "holidayObjectStore";
    var HOLIDAYS_YEARS = "holidaysYears";
    var KEY_PATH = "id";

    function connectHolidaysDB(f) {
        var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        var request = indexedDB.open("holidays", 1);

        request.onerror = function (err) {
            console.log(err);
        };

        request.onsuccess = function () {
            f(request.result);
        };

        request.onupgradeneeded = function (e) {
            var objectStore = e.currentTarget.result.createObjectStore(OBJECT_STORE_NAME, { keyPath: KEY_PATH, autoIncrement: true });
            objectStore.createIndex("date", "date", { unique: false });
            connectHolidaysDB(f);
        };
    }

    this.getHolidaysByDates = function (momentDateStart, momentDateEnd) {
        var deferred = $q.defer();

        connectHolidaysDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readonly");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);
            var datesRange = IDBKeyRange.bound(momentDateStart.toDate().getTime(), momentDateEnd.toDate().getTime());
            var index = objectStore.index("date");

            index.getAll(datesRange).onsuccess = function (eventsResult) {
                deferred.resolve(eventsResult.target.result);
            };
        });

        return deferred.promise;
    };

    this.hasSavedHolidayEventsByYear = function (year) {
        var holidaysYears = localStorage.getItem(HOLIDAYS_YEARS) || null;
        if (holidaysYears === null) {
            return false;
        }

        holidaysYears = JSON.parse(holidaysYears);
        return holidaysYears.indexOf(year) !== -1;
    };

    this.clearHolidays = function () {
        connectHolidaysDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);
            objectStore.clear();
            localStorage.removeItem(HOLIDAYS_YEARS);
        });
    };

    this.addYearHolidays = function (holidays, year) {

        if (this.hasSavedHolidayEventsByYear(year))
            return;

        connectHolidaysDB(db => {
            var transaction = db.transaction([OBJECT_STORE_NAME], "readwrite");
            var objectStore = transaction.objectStore(OBJECT_STORE_NAME);

            var holidaysYears = localStorage.getItem(HOLIDAYS_YEARS) || null;

            if (holidaysYears === null) {
                holidaysYears = [];
            } else {
                holidaysYears = JSON.parse(holidaysYears);
            }

            if (holidaysYears.indexOf(year) !== -1)
                return;

            holidaysYears.push(year);
            localStorage.setItem(HOLIDAYS_YEARS, JSON.stringify(holidaysYears));

            holidays.forEach(function (holiday) {
                objectStore.put(holiday);
            });
        });
    };
});







