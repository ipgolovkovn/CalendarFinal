﻿app.service("UserEventsService", function ($http, $httpParamSerializerJQLike, AuthService, UserEventsStorageService) {

    this.getAllUserEvents = function () {
        return $http({
                url: "/api/Event/GetAllUserEvents",
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + AuthService.getToken()
                }
            }) 
            .then(
                function success(response) {
                    return response.data.map(convertEventFromServer);
                },
                function error(response) {
                    AuthService.checkResponse(response);
                }   
            );
    };

    this.getEventsByDates = function (momentDateStart, momentDateEnd) {
        return $http({
            url: "/api/Event/GetUserEventsByDates/" + momentDateStart.format("MM-DD-YYYY") + "/" + momentDateEnd.format("MM-DD-YYYY"),
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + AuthService.getToken()
                }
            }) 
            .then(
                function success(response) {
                    var events = response.data.map(convertEventFromServer);
                    UserEventsStorageService.createOrUpdateEvents(events);
                    return events;
                },
                function error(response) {
                    if (!AuthService.checkResponse(response)) {
                        return [];
                    } else {
                        return UserEventsStorageService.getEventsByDates(momentDateStart, momentDateEnd);
                    }
                } 
            );
    };

    this.getEvent = function (id) {
        return $http({
                url: "/api/Event/GetUserEvent/" + id,
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + AuthService.getToken()
                }
            }) 
            .then(
                function success(response) {
                    var event = convertEventFromServer(response.data);
                    UserEventsStorageService.createOrUpdateEvents([event]);
                    return event;
                },
                function error(response) {
                    if (!AuthService.checkResponse(response)) {
                        return null;
                    } else {
                        return UserEventsStorageService.getEvent(id);
                    }
                }
            );
    };

    this.addEvent = function (event) {
        return $http({
            url: "/api/Event/AddNewEvent",
            method: 'POST',
            data: convertEventToServer(event),
            headers: {
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        }).then(
            function success(response) {
                var event = convertEventFromServer(response.data);
                UserEventsStorageService.createOrUpdateEvents([event]);
                return event;
            },
            function error(response) {
                return null;
            }
        );
    };

    this.saveEvent = function (event) {
        return $http({
            url: "/api/Event/EditEvent/" + event.id,
            method: 'POST',
            data: convertEventToServer(event),
            headers: {
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        }).then(
            function success(response) {
                var event = convertEventFromServer(response.data);
                UserEventsStorageService.createOrUpdateEvents([event]);
                return event;
            },
            function error(response) {
                return null;
            }
        );
    };

    this.deleteEvent = function (id) {
        return $http({
            url: "/api/Event/DeleteEvent/" + id,
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + AuthService.getToken()
            }
        }).then(
            function success(response) {
                UserEventsStorageService.deleteEvent(id);
                return response.data;
            },
            function error(response) {
                return null;
            }
        );
    };

    function convertEventFromServer(event) {
        event.dateStart = moment(event.dateStart).toDate().getTime();
        event.dateEnd = moment(event.dateEnd).toDate().getTime();
        return event;
    }

    function convertEventToServer(event) {
        event.dateStart = moment(event.dateStart).toDate();
        event.dateEnd = moment(event.dateEnd).toDate();
        return event;
    }

});