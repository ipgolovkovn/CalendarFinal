﻿var app = angular.module("calendar", ["ngRoute", "angularjs-datetime-picker"]);

app.config(function ($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'CalendarApp/Views/Week.html',
            controller: 'WeekController'
        })
          
        .when('/auth', {
            templateUrl: 'CalendarApp/Views/Auth.html',
            controller: 'AuthController'
        })

        .when('/register', {
            templateUrl: 'CalendarApp/Views/Register.html',
            controller: 'AuthController'
        })

        .when('/day', {
            templateUrl: 'CalendarApp/Views/Day.html',
            controller: 'DayController'
        })

        .when('/day/:year/:month/:day', {
            templateUrl: 'CalendarApp/Views/Day.html',
            controller: 'DayController'
        })

        .when('/week', {
            templateUrl: 'CalendarApp/Views/Week.html',
            controller: 'WeekController'
        })

        .when('/week/:year/:month/:day', {
            templateUrl: 'CalendarApp/Views/Week.html',
            controller: 'WeekController'
        })

        .when('/month', {
            templateUrl: 'CalendarApp/Views/Month.html',
            controller: 'MonthController'
        })

        .when('/month/:year/:month/:day', {
            templateUrl: 'CalendarApp/Views/Month.html',
            controller: 'MonthController'
        })

        .when('/year/:year/:month/:day', {
            templateUrl: 'CalendarApp/Views/Year.html',
            controller: 'YearController'
        })

        .when('/year', {
            templateUrl: 'CalendarApp/Views/Year.html',
            controller: 'YearController'
        })

        .when('/event/edit/:ID', {
            templateUrl: 'CalendarApp/Views/UserEvent.html',
            controller: 'UserEventController'
        })

        .when('/event/create', {
            templateUrl: 'CalendarApp/Views/UserEvent.html',
            controller: 'UserEventController'
        })

        .otherwise({
            templateUrl: 'CalendarApp/Views/NotFound.html',
            controller: 'NotFoundController'
        });

});

moment.updateLocale('en', {
    weekdaysMin: [
        "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"
    ]
});

