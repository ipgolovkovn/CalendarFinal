﻿app.controller("DayController", function ($scope, $routeParams, $location, HolidayService, HelperService, UserEventsService) {

    var date = new Date();
    if ($routeParams.year !== undefined) {
        date = new Date($routeParams.year, $routeParams.month - 1, $routeParams.day);
    }

    $scope.date = date;

    var momentDate = moment(date);
    var momentDateStart = momentDate.clone().startOf('day');
    var momentDateEnd = momentDate.clone().endOf('day');

    UserEventsService.getEventsByDates(momentDateStart, momentDateEnd).then(updateUserEvents);
    HolidayService.getHolidaysByDates(momentDateStart, momentDateEnd).then(updateHolidayparams);

    var gridData = {};
    for (var hour = 0; hour < 24; hour++) {
        gridData[hour] = {};
    }

    $scope.gridData = gridData;
    $scope.dayTitle = date.getDate();
    $scope.monthAndYearTitle = moment.monthsShort()[momentDate.month()] + " " + momentDate.year();

    $scope.goToPrevDay = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.subtract(1, "day");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "day"));
    };

    $scope.goToNextDay = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.add(1, "day");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "day"));
    };

    $scope.editEvent = function (id) {
        $location.path("/event/edit/" + id);
    };

    function updateUserEvents(events) {
        events.forEach(function (event) {
            var momentDateStart = moment(HelperService.clone(event.dateStart));
            var hourParams = $scope.gridData[momentDateStart.clone().startOf('hour').hour()];
            var hourEvents = hourParams.events || [];
            hourEvents.push({
                id: event.id,
                text: HelperService.getEventShortText(event, 200, ["name", "description"]),
                sortTime: event.dateStart
            });
            hourParams.events = hourEvents;
        });

        sortUserEvents();
    }

    function sortUserEvents() {
        for (var hourKey in $scope.gridData) {
            var hourParams = $scope.gridData[hourKey];
            if (hourParams.events !== undefined) {
                hourParams.events.sort(function (first, second) {
                    return first.sortTime - second.sortTime;
                });
            }
        }
    }

    function updateHolidayparams(holidays) {
        $scope.holiday = holidays[0];
    }

});