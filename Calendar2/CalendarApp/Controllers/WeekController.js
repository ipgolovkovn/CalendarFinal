﻿app.controller("WeekController", function ($scope, HelperService, UserEventsService, HolidayService, $location, $routeParams) {
    
    var date = new Date();
    if ($routeParams.year !== undefined) {
        date = new Date($routeParams.year, $routeParams.month - 1, $routeParams.day);
    }

    $scope.date = date;
    $scope.holidays = [];

    var momentDate = moment(date);
    var firstDateOfWeek = momentDate.clone().startOf('isoweek');
    var lastDateOfWeek = momentDate.clone().endOf('isoweek');

    UserEventsService.getEventsByDates(firstDateOfWeek, lastDateOfWeek).then(updateUserEvents);
    HolidayService.getHolidaysByDates(firstDateOfWeek, lastDateOfWeek).then(updateHolidays);

    var gridData = {};
    var loopDate;
    for (var hour = 0; hour < 24; hour++) {
        loopDate = firstDateOfWeek.clone();
        gridData[hour] = {};
        while (loopDate <= lastDateOfWeek) {
            gridData[hour][HelperService.getKeyByDate(loopDate.toDate())] = {events: []};
            loopDate.add(1, 'day');
        }
    }

    var gridTitles = [];
    loopDate = firstDateOfWeek.clone();
    while (loopDate <= lastDateOfWeek) {
        gridTitles.push(loopDate.date() + " " + moment.monthsShort()[loopDate.month()] + ", " + moment.weekdaysMin()[loopDate.isoWeekday() - 1]);
        loopDate.add(1, 'day');
    }

    $scope.gridData = gridData;
    $scope.gridTitles = gridTitles;
    $scope.navigateWeekTitle = firstDateOfWeek.date() + " " + moment.monthsShort()[firstDateOfWeek.month()] + " - " + lastDateOfWeek.date() + " " + moment.monthsShort()[lastDateOfWeek.month()];

    $scope.goToPrevWeek = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.subtract(1, "week");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "week"));
    };

    $scope.goToNextWeek = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.add(1, "week");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "week"));
    };

    $scope.editEvent = function (id) {
        $location.path("/event/edit/" + id);
    };

    function updateUserEvents(events) {
        events.forEach(function (event) {
            var momentDateStart = moment(event.dateStart);
            var dateHourParams = $scope.gridData[momentDateStart.startOf('hour').hour()][HelperService.getKeyByDate(momentDateStart.toDate())];
            var dateHourEvents = dateHourParams.events || [];
            dateHourEvents.push({
                id: event.id,
                text: HelperService.getEventShortText(event, 15, ["name"]),
                minutes: momentDateStart.minutes(),
                sortTime: event.dateStart
            });
            dateHourParams.events = dateHourEvents;
        });

        sortUserEvents();
    }

    function sortUserEvents() {
        for (var hourKey in $scope.gridData) {
            for (var dayKey in $scope.gridData[hourKey]) {
                var day = $scope.gridData[hourKey][dayKey];
                if (day.events !== undefined) {
                    day.events.sort(function (first, second) {
                        return first.sortTime - second.sortTime;
                    });
                }
            }
        }
    }

    function updateHolidays(holidays) {
        holidays.forEach(function (holiday) {
            var momentDate = moment(holiday.date);
            $scope.holidays[momentDate.isoWeekday() - 1] = holiday;
        });
    }
   
});