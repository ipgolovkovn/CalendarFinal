﻿app.controller("YearController", function ($location, $routeParams, $scope, HelperService, UserEventsService, HolidayService) {
    var MONTH_COLS_CNT = 4;
    var MONTH_ROWS_CNT = 3;

    var date = new Date();
    if ($routeParams.year !== undefined) {
        date = new Date($routeParams.year, $routeParams.month - 1, $routeParams.day);
    }

    $scope.date = date;
    var year = date.getFullYear();

    var monthsArray = [];
    var weeksArray, curWeek, firstDateOfStartWeekInMonth, lastDateOfLastWeekInMonth;
    var curMonth = 0;

    for (var row = 0; row < MONTH_ROWS_CNT; row++) {
        monthsArray[row] = [];
        for (var col = 0; col < MONTH_COLS_CNT; col++) {
            var momentFirstDayOfMonth = moment([year, curMonth, 1]);
            firstDateOfStartWeekInMonth = HelperService.getFirstDateOfStartWeekInMonth(momentFirstDayOfMonth);
            lastDateOfLastWeekInMonth = HelperService.getLastDateOfLastWeekInMonth(momentFirstDayOfMonth);

            weeksArray = [];
            curWeek = 0;
            var loopDate = firstDateOfStartWeekInMonth.clone();
            while (loopDate <= lastDateOfLastWeekInMonth) {
                weeksArray[curWeek] = [];

                for (var weekDay = 0; weekDay < 7; weekDay++) {
                    var dayParams = { day: loopDate.clone().toDate()};

                    if (loopDate.month() !== curMonth) {
                        dayParams.notActive = true;
                    }

                    weeksArray[curWeek].push(dayParams);

                    loopDate.add(1, 'day');
                }
                curWeek++;
            }

            monthsArray[row][col] = weeksArray;
            curMonth++;
        }
    }

    var firstDateInYearRange = HelperService.getFirstDateOfStartWeekInMonth(moment([year]).startOf("year"));
    var lastDateInYearRange = HelperService.getLastDateOfLastWeekInMonth(moment([year]).endOf("year"));

    UserEventsService.getEventsByDates(firstDateInYearRange, lastDateInYearRange).then(updateUserEvents);
    HolidayService.getHolidaysByDates(firstDateInYearRange, lastDateInYearRange).then(updateHolidays);

    $scope.weekDays = moment.weekdaysMin();
    $scope.months = moment.monthsShort();
    $scope.gridData = monthsArray;

    $scope.goToPrevYear = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.subtract(1, "year");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "year"));
    };

    $scope.goToNextYear = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.add(1, "year");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "year"));
    };

    $scope.goToChosenDay = function (date) {
        $location.path(HelperService.getLinkPathByDate(date, "day"));
    };

    function getParamsForUpdate(item, dateParamName) {
        var momentDate = moment(item[dateParamName]);
        var month = momentDate.month();
        var monthRow = Math.floor(month / MONTH_COLS_CNT);
        var monthCol = (month - (monthRow * MONTH_COLS_CNT)) % MONTH_COLS_CNT;
        var weekNumber = HelperService.getWeekNumberInMonthByDate(momentDate, momentDate.year(), month + 1);
        var dayNumber = momentDate.isoWeekday() - 1;
        return {
            "monthRow": monthRow,
            "monthCol": monthCol,
            "weekNumber": weekNumber,
            "dayNumber": dayNumber
        };
    }

    function updateUserEvents(events) {
        events.forEach(function (event) {
            var params = getParamsForUpdate(event, "dateStart");
            $scope.gridData[params.monthRow][params.monthCol][params.weekNumber][params.dayNumber].hasEvents = true;
        });
    }

    function updateHolidays(holidays) {
        holidays.forEach(function (holiday) {
            var params = getParamsForUpdate(holiday, "date");
            $scope.gridData[params.monthRow][params.monthCol][params.weekNumber][params.dayNumber].holiday = holiday;
        });
    }

});

