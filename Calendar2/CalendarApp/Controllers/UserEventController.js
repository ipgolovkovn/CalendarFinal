﻿app.controller("UserEventController", function ($scope, $routeParams, $location, UserEventsService, HelperService) {

    var id = $routeParams.ID;
    if (id === undefined) {
        $scope.event = {};
    } else {
        UserEventsService.getEvent(id).then(function (event) {
            var eventClone = JSON.parse(JSON.stringify(event));
            eventClone.dateStart = moment(eventClone.dateStart).format("YYYY-MM-DD HH:mm");
            eventClone.dateEnd = moment(eventClone.dateEnd).format("YYYY-MM-DD HH:mm");
            $scope.event = eventClone;
        });
    }

    $scope.save = function (valid) {
        if (valid) {
            if ($scope.event.id === undefined) {
                UserEventsService.addEvent($scope.event).then(
                    function () {
                        $location.path(HelperService.getLinkPathByDate($scope.event.dateStart, "day"));
                    }
                );

            } else {
                UserEventsService.saveEvent($scope.event).then(
                    function () {
                        $location.path(HelperService.getLinkPathByDate($scope.event.dateStart, "day"));
                    }
                );
            }
        }
    };

    $scope.delete = function () {
        UserEventsService.deleteEvent($scope.event.id).then(
            function () {
                $location.path("/week");
            }
        );
    };
        
});