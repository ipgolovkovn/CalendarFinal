﻿app.controller("MonthController", function ($location, $scope, $routeParams, HelperService, UserEventsService, HolidayService) {

    var date = new Date();
    if ($routeParams.year !== undefined) {
        date = new Date($routeParams.year, $routeParams.month - 1, $routeParams.day);
    }

    $scope.date = date;

    var momentDate = moment(date);
    var firstDateOfStartWeekInMonth = HelperService.getFirstDateOfStartWeekInMonth(momentDate);
    var lastDateOfLastWeekInMonth = HelperService.getLastDateOfLastWeekInMonth(momentDate);

    UserEventsService.getEventsByDates(firstDateOfStartWeekInMonth, lastDateOfLastWeekInMonth).then(updateUserEvents);
    HolidayService.getHolidaysByDates(firstDateOfStartWeekInMonth, lastDateOfLastWeekInMonth).then(updateHolidays);

    var gridData = {};
    var loopDate = firstDateOfStartWeekInMonth.clone();
    var week = 0;
    var curMonth = date.getMonth();
    while (loopDate <= lastDateOfLastWeekInMonth) {
        gridData[week] = {};

        for (var weekDay = 0; weekDay < 7; weekDay++) {
            gridData[week][HelperService.getKeyByDate(loopDate.toDate())] = { date: loopDate.toDate(), active: (loopDate.get("month") === curMonth) };
            loopDate.add(1, 'day');
        }

        week++;
    }

    $scope.gridData = gridData;
    $scope.weekDays = moment.weekdaysMin();
    $scope.navigateMonthTitle = moment.months()[date.getMonth()];
    $scope.firstDateOfStartWeekInMonth = firstDateOfStartWeekInMonth;

    $scope.goToPrevMonth = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.subtract(1, "month");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "month"));
    };

    $scope.goToNextMonth = function () {
        var currentDate = moment($scope.date);
        var newDate = currentDate.add(1, "month");
        $location.path(HelperService.getLinkPathByDate(newDate.toDate(), "month"));
    };

    $scope.addEvent = function () {
        $location.path("/event/create");
    };

    $scope.editEvent = function (id) {
        $location.path("/event/edit/" + id);
    };

    $scope.goToDay = function (date) {
        $location.path(HelperService.getLinkPathByDate(date, "day"));
    };

    function addUserEventOrHoliday(item, dateParamName, addFunction) {
        var momentDate = moment(item[dateParamName]);
        var weekNumber = HelperService.getWeekNumberInMonthByDate(momentDate, $scope.date.getFullYear(), $scope.date.getMonth() + 1);
        var dateParams = $scope.gridData[weekNumber][HelperService.getKeyByDate(momentDate.toDate())];

        addFunction(dateParams);
    }

    function sortUserEvents() {
        for (var weekNumber in $scope.gridData) {
            for (var dayKey in $scope.gridData[weekNumber]) {
                var day = $scope.gridData[weekNumber][dayKey];
                if (day.events !== undefined) {
                    day.events.sort(function (first, second) {
                        return first.sortTime - second.sortTime;
                    });
                }
            }
        }
    }

    function updateUserEvents(events) {
        events.forEach(function (event) {
            addUserEventOrHoliday(event, "dateStart", function (dateParams) {
                var dayEvents = dateParams.events || [];
                dayEvents.push({
                    id: event.id,
                    text: HelperService.getEventShortText(event, 25, ["name"]),
                    sortTime: event.dateStart
                });
                dateParams.events = dayEvents;
            });
        });

        sortUserEvents();
    }

    function updateHolidays(holidays) {
        holidays.forEach(function (holiday) {
            addUserEventOrHoliday(holiday, "date", function (dateParams) {
                dateParams.holiday = holiday;
            });
        });
    }

});